REM KiCadPythonActionsPlugins windowsinstall.sh
REM coding UTF-8 Windows(CR LF)

REM 1. 先确定当前所在绝对路径，再确定需要安装的路径。

REM 2. 检测需要使用的命令行是否存在

REM 3. 初始化并更新子模块仓库中的插件

REM 4. 对需要安装依赖包子模块仓库安装相应的 Python 依赖包

REM 5. 记录子模块的仓库的版本信息
REM 使用 git describe --always | sed 's|-|.|g'
REM 记录软件包版本信息并保存到当前路径下 pluginsinfo.txt

REM 6. 自动安装到系统中 KiCad 的插件路径
REM 稳定版安装路径：/usr/share/kicad/scripting/plugins/
REM 测试版安装路径：/usr/share/kicad/5.99/scripting/plugins/

REM 7. 安装插件软件包版本信息 pluginsinfo.txt 到相应的插件安装位置
REM 稳定版：/usr/share/kicad/scripting/plugins/pluginsinfo.txt
REM 测试版：/usr/share/kicad/5.99/scripting/plugins/pluginsinfo.txt

REM 8. 自动更新子模块仓库并检测记录的版本信息是否一致
REM 一致不更新
REM 不一致更新

REM 9. 卸载 KiCad Python 子模块插件
REM 从插件信息里面读取安装的插件进行卸载或删除